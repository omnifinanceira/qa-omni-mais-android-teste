Dado("que eu possua um ficha com status {string}") do |status|
  conn = OCI8.new("supervisor", "omnidev", "10.20.1.111:1522/omnidev")
  query = "SELECT DISTINCT LOCAL, status, proposta FROM proposta p
  WHERE EXISTS (SELECT 1 FROM rastreio_proposta r 
  WHERE r.proposta = p.proposta AND sequencia = (SELECT Max(sequencia) 
  FROM rastreio_proposta r2 WHERE r.proposta = r2.proposta 
  ) 
  AND cod_status = #{status}
  ) 
  AND emissao BETWEEN SYSDATE - 15 AND SYSDATE
  AND agente = '184'"
  @teste = []
  cursor = conn.parse(query)
  cursor.exec
  cursor.fetch() { |row| @teste << row[2] }
  puts @proposta = @teste[0].to_i
  raise "Nao houve propostas no status #{status}, nos ultimos dias" if @proposta == 0
  #binding.pry
end

Quando("eu pesquisar a ficha") do
  @api.vai_volta
  find_element(id: "menu_home_filter").click
  @api.vai_volta
  find_element(id: "act_proposal_filter_et_proposal_id").send_keys(@proposta)
  find_element(id: "act_proposal_filter_btn_apply").click
end

Entao("eu espero visualizar ela no status {string}") do |string|
  nuzei = find_element(id: "item_proposal_summary_rl_status")
  retorno = nuzei.xpath("//..")
  expect(retorno.find_element(id: "item_proposal_summary_tv_status").text).to eql(string)
  expect(retorno.find_element(id: "item_proposal_summary_tv_id").text).to eql(@proposta.to_s)
end
