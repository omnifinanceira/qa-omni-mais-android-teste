Dado("que eu realize o login com usuario {string}") do |string|
  find_element(:id, "act_login_et_login").send_keys(MASSA[string]["user"])
  find_element(:id, "act_login_et_password").send_keys(MASSA[string]["pass"])
  find_element(:id, "act_login_btn_login").click
end

Dado("eu esteja na tela inicial") do
  text("MINHAS FICHAS")
end

Quando("eu clicar no {string}") do |string|
  find_element(:id, ELEMENTOS[string]).click
end

Quando("eu selecionar {string} como {string}") do |string, string2|
  find_element(:id, ELEMENTOS[string][string2]).click
end

Quando("eu escolher {string} como {string}") do |string, string2|
  Appium::TouchAction.new.tap(x: 0.50, y: 0.40).perform
  el1 = find_element(:id, ELEMENTOS[string]["campo"])
  el1.send_keys(string2)
  sleep 1
  Appium::TouchAction.new.tap(x: 0.50, y: 0.45).perform
  find_element(:id, ELEMENTOS[string]["botao"]).click
  sleep 2
end

Quando("eu escolher o vendedor") do
  @api.vai_volta
  el1 = find_element(:id, ELEMENTOS["NOME DO VENDEDOR"]["campo"])
  el1.send_keys("sou")
  sleep 1
  Appium::TouchAction.new.tap(x: 0.50, y: 0.45).perform
  find_element(:id, ELEMENTOS["NOME DO VENDEDOR"]["botao"]).click
  sleep 2
end

Quando("eu digitar o cpf do cliente como {string}") do |string|
  @api.vai_volta
  find_element(:id, "frag_financing_form_customer_cpf_et_cpf").send_keys(MASSA[string])
  find_element(:id, "ic_financing_form_cpf_btn_next").click
  # ignora a recuperação de ficha, caso ela apareca
  if find_elements(:id, "dlg_recovery_client_info_tv_title").empty? == false
    find_element(:id, "dlg_recovery_client_info_btn_do_not_recover").click
  end
end

Quando("eu preencher a data de nascimento {string}") do |string|
  find_element(:id, "frag_financing_form_birth_date_et_date").send_keys(string)
  find_element(:id, "frag_financing_form_birth_date_btn_next").click
end

Quando("eu preencher o celular do cliente {string}") do |string|
  Appium::TouchAction.new.tap(x: 0.50, y: 0.40).perform
  sleep 2
  find_element(:id, "frag_financing_form_cell_phone_et_cell_phone").send_keys(string)
  find_element(:id, "frag_financing_form_cell_phone_btn_next").click
end

Quando("eu preencher a renda comprovada {string}") do |string|
  find_element(:id, "frag_financing_form_income_et_income").send_keys(string)
  find_element(:id, "frag_financing_form_income_btn_next").click
end

Quando("eu preencher a placa do veiculo {string}") do |string|
  find_element(:id, "frag_financing_form_board_et_board").send_keys(string)
  find_element(:id, "frag_financing_form_board_btn_next").click
end

Quando("eu selecionar o estado de licenciamento como {string}") do |string|
  el1 = find_element(:id, "frag_new_record_licensing_atv_state")
  el1.send_keys(string)
  sleep 1
  Appium::TouchAction.new.tap(x: 0.50, y: 0.44).perform
  find_element(:id, "frag_new_record_licensing_btn_next").click
  sleep 2
end

Quando("eu aguardo o resultado") do
  #wait_true { ignore { find_element(:id, "frag_financing_form_loading_iv_hourglass").nil? } }
  @wait.until { text("AVISO") }
  p "achou"
  find_element(:id, "dlg_one_button_btn_neutral").click
end

Entao("eu espero visualizar a proposta na tela inicial com o header Resultado Parcial") do
  text("MINHAS FICHAS")
  elemento = find_element(:xpath, "//android.widget.TextView[@text='RESULTADO PARCIAL']")
  elemento = elemento.xpath("//../..")
  cpf = elemento.id("item_proposal_summary_tv_client_document").text.delete("^0-9")
  expect(cpf).to eql(MASSA["cpf valido"])
end
