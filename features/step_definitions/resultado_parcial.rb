Dado("que eu possua uma ficha no status Resultado parcial {string}") do |string|
resposta = @api.cria_ficha(string)
@proposta = resposta["proposta"]["id"]
end

Dado("que eu possua uma ficha no status Resultado parcial {string} com fipe nao encontrada") do |string|
  resposta = @api.cria_ficha_fipe_not_found(string)
  @proposta = resposta["proposta"]["id"]
  end
##################################################################
Quando("eu selecionar a proposta no status Resultado Parcial") do
  ele = text(@proposta.to_s)
  pai = ele.xpath("//..")
  pai.find_element(:id, "item_proposal_summary_tv_status").click
end

Quando("eu selecionar o status Resultado Parcial") do
  text("RESULTADO PARCIAL").click
end

Quando("eu validar os dados da proposta") do
  if find_elements(id: "dlg_two_buttons_btn_positive").size > 0
    find_element(id: "dlg_two_buttons_btn_positive").click
  end
  @api.vai_volta
  find_element(:id, "frg_partial_result_btn_continue").click
end

Quando("eu confirmar o cliente") do
  find_element(id: "frag_financing_form_customer_name_btn_next").click
end

Quando("eu preencher o email com {string}") do |string|
  find_element(:id, ELEMENTOS["email"]).send_keys(string)
  find_element(:id, "frag_financing_form_email_btn_next").click
end

Quando("eu selecionar o estado civil como {string}") do |string|
  text(string).click
end

Quando("eu preencher os dados de Identificacao") do
  find_element(:id, "frag_new_record_identification_atv_document").click
  Appium::TouchAction.new.tap(x: 0.50, y: 0.75).perform
  find_element(:id, "frag_new_record_identification_atv_document_number").send_keys("494035783")
  find_element(:id, "frag_new_record_identification_atv_document_entity").send_keys("ssp")
  find_element(:id, "frag_new_record_identification_atv_document_expedition").send_keys("12/03/2015")
  sleep 0.2
  find_element(:id, "frag_new_record_identification_btn_next").click
end

Quando("eu selecionar a nacionalidade") do
 @api.vai_volta
  find_element(:id, "frag_financing_form_country_birth_atv_country").send_keys("sp")
  sleep 0.2
  Appium::TouchAction.new.tap(x: 0.50, y: 0.50).perform
  find_element(:id, "frag_financing_form_country_birth_atv_cities").send_keys("AJOXJ")
  sleep 0.2
  Appium::TouchAction.new.tap(x: 0.50, y: 0.53).perform
  find_element(:id, "frag_financing_form_country_birth_btn_next").click
end

Quando("eu selecionar uma classe profissional como {string}") do |string|
@api.vai_volta
  text(string).click
end

Quando("eu selecionar a profissao do cliente") do
@api.vai_volta
  find_element(:id, "frag_new_record_profession_atv_profession").send_keys("advogado")
  sleep 0.5
  Appium::TouchAction.new.tap(x: 0.50, y: 0.40).perform
  find_element(:id, "frag_new_record_profession_btn_next").click
end

Quando("eu preencher o {string} com {string}") do |string, string2|
@api.vai_volta
  find_element(:id, ELEMENTOS[string]["campo"]).send_keys(string2)
  find_element(:id, ELEMENTOS[string]["botao"]).click
end

Quando("eu selecionar a opcao de politicamente exposto") do
  text("Não").click
  find_element(:id,"frag_new_record_politically_exposed_btn_next").click
end

Quando("eu selecionar o endereco de correspondencia") do
  text("Residencial").click  
end

Quando("eu nao inserir um telefone adicional") do
  find_element(:id, "frag_financing_form_optional_phone_btn_next").click
end

Quando("eu selecionar a relacao da referencia como {string}") do |string|
  text(string).click
end

Quando("eu nao preencher o renavam") do
  find_element(:id, "frag_financing_form_renavam_btn_next").click
end
Entao("eu espero visualizar a tela de conclusao") do
  text("A FICHA #{@proposta.to_s} FOI FINALIZADA E ENVIADA PARA ANÁLISE")
end

Quando("eu selecionar a opcao {string}") do |string|
@api.vai_volta
  text(string).click
end

Quando("eu valido o valor do veiculo") do
@api.vai_volta
  @valor_fipe = find_element(id: "frag_financing_form_vehicle_value_tv_value").text
  find_element(id:"frag_financing_form_vehicle_value_btn_next").click
end

Quando("eu preencher o campo {string} com {string}") do |string, string2|
@api.vai_volta
  find_element(:id, ELEMENTOS[string]["campo"]).send_keys(string2)
  sleep 0.5
  Appium::TouchAction.new.tap(x: 0.50, y: 0.40).perform  
  find_element(:id, ELEMENTOS[string]["botao"]).click
end

Quando("eu preencher o nome da mae com {string}") do |string|
  find_element(id:"frag_financing_form_mother_name_tv_name_not_found").click if find_elements(id:"frag_financing_form_mother_name_tv_name_not_found").size > 0
  find_element(id: "frag_financing_form_mother_text_name_et_name").send_keys(string)
  find_element(id: "frag_financing_form_mother_text_name_btn_next").click
end