#language: pt
@resultado_parcial
Funcionalidade: Caminho feliz - Resultado Parcial

   Como usuario, Eu desejo que uma ficha chegue no resultado parcial para ser complementada com as informações pos analise 
@enviar_analise_cdc
   Cenario: Enviar a ficha para analise com sucesso - CDC
   Dado que eu possua uma ficha no status Resultado parcial "CDC"
   E que eu realize o login com usuario "184TIAGO"
   E eu esteja na tela inicial
	Quando eu selecionar a proposta no status Resultado Parcial
	E eu selecionar o status Resultado Parcial
	E eu validar os dados da proposta
	E eu confirmar o cliente
	E eu preencher o email com "victor.debaza@omni.com.br"
	E eu selecionar o estado civil como "CASADO"
	E eu preencher os dados de Identificacao
	E eu preencher o nome da mae com "mae da automacao"
	E eu preencher o "nome do pai" com "pai da automacao"
	E eu selecionar a nacionalidade
	E eu preencher o "CEP da Residencia" com "05780230"
	E eu preencher o "complemento do endereco" com "55"
	E eu selecionar uma classe profissional como "EMPREGADO"
	E eu selecionar a profissao do cliente
	E eu preencher o "nome da empresa" com "Omni Automacoes"
	E eu preencher o "telefone da empresa" com "1154585585"
	E eu preencher o "cep da empresa" com "05633010"
	E eu preencher o "complemento do endereco da empresa" com "223"
	E eu preencher o "total do patrimonio" com "10000000"
	E eu selecionar a opcao de politicamente exposto 
	E eu selecionar o endereco de correspondencia
	Mas eu nao inserir um telefone adicional
	E eu preencher o "referencia" com "referencia automacao"
	E eu preencher o "contato da referencia" com "1154844587"
	E eu selecionar a relacao da referencia como "Parente"
	Mas eu nao preencher o renavam
	E eu preencher o "observacoes" com "Teste automatizado"
	Entao eu espero visualizar a tela de conclusao

	@enviar_analise_cdc_fipe_not_found
   Cenario: Enviar a ficha para analise com sucesso - CDC
    Dado que eu possua uma ficha no status Resultado parcial "CDC" com fipe nao encontrada
    E que eu realize o login com usuario "184TIAGO"
    E eu esteja na tela inicial
	Quando eu selecionar a proposta no status Resultado Parcial
	E eu selecionar a opcao "AUTOMOVEL"
	E eu preencher o campo "MARCA DO VEÍCULO" com "ford"
	E eu preencher o campo "ANO DO VEÍCULO" com "2006"
	E eu preencher o campo "MODELO DO VEÍCULO" com "ka"
	E eu preencher o campo "VERSÃO DO VEÍCULO" com "action"
	E eu preencher o campo "COR DO VEÍCULO" com "azul"
	E eu preencher o campo "TIPO DE COMBUSTÍVEL" com "gasolina"
	E eu valido o valor do veiculo
	E eu preencher o campo "ESTADO DE LICENCIAMENTO" com "SP"	
	E eu aguardo o resultado	
	E eu selecionar a proposta no status Resultado Parcial
	E eu selecionar o status Resultado Parcial
	E eu validar os dados da proposta
	E eu confirmar o cliente
	E eu preencher o email com "victor.debaza@omni.com.br"
	E eu selecionar o estado civil como "CASADO"
	E eu preencher os dados de Identificacao
	E eu preencher o nome da mae com "mae da automacao"
	E eu preencher o "nome do pai" com "pai da automacao"
	E eu selecionar a nacionalidade
	E eu preencher o "CEP da Residencia" com "05780230"
	E eu preencher o "complemento do endereco" com "55"
	E eu selecionar uma classe profissional como "EMPREGADO"
	E eu selecionar a profissao do cliente
	E eu preencher o "nome da empresa" com "Omni Automacoes"
	E eu preencher o "telefone da empresa" com "1154585585"
	E eu preencher o "cep da empresa" com "05633010"
	E eu preencher o "complemento do endereco da empresa" com "223"
	E eu preencher o "total do patrimonio" com "10000000"
	E eu selecionar a opcao de politicamente exposto 
	E eu selecionar o endereco de correspondencia
	Mas eu nao inserir um telefone adicional
	E eu preencher o "referencia" com "referencia automacao"
	E eu preencher o "contato da referencia" com "1154844587"
	E eu selecionar a relacao da referencia como "Parente"
	Mas eu nao preencher o renavam
	E eu preencher o "observacoes" com "Teste automatizado"
	Entao eu espero visualizar a tela de conclusao

@refinanciamento
Cenario: Enviar a ficha para analise com sucesso - CDC
    Dado que eu possua uma ficha no status Resultado parcial "REFINANCIAMENTO"
	E que eu realize o login com usuario "184TIAGO"
	E eu esteja na tela inicial
	Quando eu selecionar a proposta no status Resultado Parcial
	E eu selecionar o status Resultado Parcial
	E eu validar os dados da proposta
	E eu confirmar o cliente
	E eu preencher o email com "victor.debaza@omni.com.br"
	E eu selecionar o estado civil como "CASADO"
	E eu preencher os dados de Identificacao
	E eu preencher o nome da mae com "mae da automacao"
	E eu preencher o "nome do pai" com "pai da automacao"
	E eu selecionar a nacionalidade
	E eu preencher o "CEP da Residencia" com "05780230"
	E eu preencher o "complemento do endereco" com "55"
	E eu selecionar uma classe profissional como "EMPREGADO"
	E eu selecionar a profissao do cliente
	E eu preencher o "nome da empresa" com "Omni Automacoes"
	E eu preencher o "telefone da empresa" com "1154585585"
	E eu preencher o "cep da empresa" com "05633010"
	E eu preencher o "complemento do endereco da empresa" com "223"
	E eu preencher o "total do patrimonio" com "100000"
	E eu selecionar a opcao de politicamente exposto 
	E eu selecionar o endereco de correspondencia
	Mas eu nao inserir um telefone adicional
	E eu preencher o "referencia" com "referencia automacao"
	E eu preencher o "contato da referencia" com "1154844587"
	E eu selecionar a relacao da referencia como "Parente"
	Mas eu nao preencher o renavam
	E eu preencher o "observacoes" com "Teste automatizado"
	Entao eu espero visualizar a tela de conclusao

