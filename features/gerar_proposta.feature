#language: pt
@gerar_ficha
Funcionalidade: Caminho feliz - Geracao de ficha
  Eu como um usuario desejo criar uma ficha para gerar um proposta para o cliente
  @vendedor_financiamento
  Cenario: Gerar ficha com sucesso - login omni - Solicitante Vendedor
  Dado que eu realize o login com usuario "184TIAGO"
  E eu esteja na tela inicial
  Quando eu clicar no "botao mais"
  E eu selecionar "QUEM SELECIONOU?" como "VENDEDOR"
  E eu selecionar "TIPO DE OPERACAO" como "FINANCIAMENTO"
  E eu escolher "LOJA DO VENDEDOR" como "autovan veiculos"
  E eu escolher o vendedor
  E eu digitar o cpf do cliente como "cpf valido"
  E eu preencher a data de nascimento "12/06/1985"
  E eu preencher o celular do cliente "81996036832"
  E eu preencher a renda comprovada "350000"
  E eu preencher a placa do veiculo "eaf1958"
  E eu selecionar o estado de licenciamento como "SP"
  E eu aguardo o resultado
  Entao eu espero visualizar a proposta na tela inicial com o header Resultado Parcial

  @vendedor_refinanciamento
  Cenario: Gerar ficha com sucesso - login omni - Solicitante Vendedor
  Dado que eu realize o login com usuario "184TIAGO"
  E eu esteja na tela inicial
  Quando eu clicar no "botao mais"
  E eu selecionar "QUEM SELECIONOU?" como "VENDEDOR"
  E eu selecionar "TIPO DE OPERACAO" como "REFINANCIAMENTO"
  E eu escolher "LOJA DO VENDEDOR" como "autovan veiculos"
  E eu escolher o vendedor
  E eu digitar o cpf do cliente como "cpf valido"
  E eu preencher a data de nascimento "12/06/1985"
  E eu preencher o celular do cliente "81996036832"
  E eu preencher a renda comprovada "35000000"
  E eu preencher a placa do veiculo "eaf1958"
  E eu selecionar o estado de licenciamento como "SP"
  E eu aguardo o resultado
  Entao eu espero visualizar a proposta na tela inicial com o header Resultado Parcial

	@balcao_financiamento
  Cenario: Gerar ficha com sucesso - login omni - Solicitante Vendedor
  Dado que eu realize o login com usuario "184TIAGO"
  E eu esteja na tela inicial
  Quando eu clicar no "botao mais"
  E eu selecionar "QUEM SELECIONOU?" como "CLIENTE NO BALCÃO"
  E eu selecionar "TIPO DE OPERACAO" como "FINANCIAMENTO"
  #E eu escolher "LOJA DO VENDEDOR" como "autovan veiculos"
  E eu escolher o vendedor
  E eu digitar o cpf do cliente como "cpf valido"
  E eu preencher a data de nascimento "12/06/1985"
  E eu preencher o celular do cliente "81996036832"
  E eu preencher a renda comprovada "350000"
  E eu preencher a placa do veiculo "eaf1958"
  E eu selecionar o estado de licenciamento como "SP"
  E eu aguardo o resultado
  Entao eu espero visualizar a proposta na tela inicial com o header Resultado Parcial
  @balcao_refinanciamento
  Cenario: Gerar ficha com sucesso - login omni - Solicitante Vendedor
  Dado que eu realize o login com usuario "184TIAGO"
  E eu esteja na tela inicial
  Quando eu clicar no "botao mais"
  E eu selecionar "QUEM SELECIONOU?" como "CLIENTE NO BALCÃO"
  E eu selecionar "TIPO DE OPERACAO" como "REFINANCIAMENTO"
  #E eu escolher "LOJA DO VENDEDOR" como "autovan veiculos"
  E eu escolher o vendedor
  E eu digitar o cpf do cliente como "cpf valido"
  E eu preencher a data de nascimento "12/06/1985"
  E eu preencher o celular do cliente "81996036832"
  E eu preencher a renda comprovada "350000"
  E eu preencher a placa do veiculo "eaf1958"
  E eu selecionar o estado de licenciamento como "SP"
  E eu aguardo o resultado
  Entao eu espero visualizar a proposta na tela inicial com o header Resultado Parcial
	
  @outro_financiamento
  Cenario: Gerar ficha com sucesso - login omni - Solicitante Vendedor
  Dado que eu realize o login com usuario "184TIAGO"
  E eu esteja na tela inicial
  Quando eu clicar no "botao mais"
  E eu selecionar "QUEM SELECIONOU?" como "OUTRA PESSOA"
  E eu selecionar "TIPO DE OPERACAO" como "FINANCIAMENTO"
  #E eu escolher "LOJA DO VENDEDOR" como "autovan veiculos"
  E eu escolher o vendedor
  E eu digitar o cpf do cliente como "cpf valido"
  E eu preencher a data de nascimento "12/06/1985"
  E eu preencher o celular do cliente "81996036832"
  E eu preencher a renda comprovada "350000"
  E eu preencher a placa do veiculo "eaf1958"
  E eu selecionar o estado de licenciamento como "SP"
  E eu aguardo o resultado
  Entao eu espero visualizar a proposta na tela inicial com o header Resultado Parcial
  