class BD
  def teste(status)
    conn = OCI8.new("supervisor", "omnidev", "10.20.1.111:1522/omnidev")
    query = "SELECT DISTINCT LOCAL, status, proposta FROM proposta p
    WHERE EXISTS (SELECT 1 FROM rastreio_proposta r 
    WHERE r.proposta = p.proposta AND sequencia = (SELECT Max(sequencia) 
    FROM rastreio_proposta r2 WHERE r.proposta = r2.proposta 
    ) 
    AND cod_status = #{status}
    ) 
    AND emissao BETWEEN SYSDATE - 10 AND SYSDATE"

    cursor = conn.parse(query)
    cursor.exec
    cursor.fetch() { |row| @teste << row[0]}
  end
end
