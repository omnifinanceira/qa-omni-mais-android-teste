class Api
def vai_volta
  press_keycode 187 #pressiona recent apps para essa merda funcionar
  sleep 0.3
  press_keycode 187 #pressiona recent apps para essa merda encontrar o campo
end

  def cria_ficha(string)
    #### Logar ############
    ENV["http_proxy"] = "http://proxy.omni.com.br"
    url = "https://hmg-omnimaisweb.omni.com.br/omnifacil/omni-ws/api/auth/user"
    header = {"Content-Type": "application/json"}
    payload = {"usuarioTO": {
      "plataforma": "WEB",
      "versaoAplicacao": "1.8.5",
      "idCodAplicacao": "1",
      "login": "184tiago",
      "senha": "senha123",
      "tokenPush": "",
    }}

    a = RestClient.post(url, payload.to_json, header) do |response|
      r = JSON.parse(response.body)
      @token = r["usuarioTO"]["token"]
    end

    #########criar request ##############
    url_vcl = "https://hmg-omnimais.omni.com.br/api/veiculo/simular"
    header_vcl = {
      "Accept": "application/json, text/plain, */*",
      "omni-autenticacao": "{\"login\":\"184TIAGO\",\"token\":\"#{@token}\"}",
      "Content-Type": "application/json",
    }
    payload_vcl = {
      "grupo1": string,
      "lojaId": "5596",
      "vendedorId": "0",
      "cpf": "03557431673",
      "dataNascimento": 170910000000,
      "ddd": "11",
      "celular": "976290517",
      "renda": "5000.00",
      "veiculos": [{
        "placa": "CVE0500",
        "uf": "PR",
        "utilizacaoAgropecuaria": false,
        "condicao": "USADO",
      }],
      "plataformaInclusao": "WEB",
    }
    a = RestClient.post(url_vcl, payload_vcl.to_json, header_vcl) do |response|
      r = JSON.parse(response.body)
      p @proposta = r["proposta"]["id"]
    end

    ############ esperar retorno ##################

    url_retorno = "https://hmg-omnimais.omni.com.br/api/veiculo/simulacao/#{@proposta}?ultimaDataTaxa=1538082978788&retorno=0&carencia=0&parcelas=0"

    loop do
      p "Aguardando resposta...."
      sleep 10
      a = RestClient.get(url_retorno, header_vcl) do |response|
        @r = JSON.parse(response.body)
        @status = @r["status"]
      end
      break unless @status == "ANALISE_PENDENTE" or @status.include?("não permitida")
    end
    puts @status
    puts @r['message']
    return @r
  end

  def cria_ficha_fipe_not_found(string)
    #### Logar ############
    ENV["http_proxy"] = "http://proxy.omni.com.br"
    url = "https://hmg-omnimaisweb.omni.com.br/omnifacil/omni-ws/api/auth/user"
    header = {"Content-Type": "application/json"}
    payload = {"usuarioTO": {
      "plataforma": "WEB",
      "versaoAplicacao": "1.8.5",
      "idCodAplicacao": "1",
      "login": "184tiago",
      "senha": "senha123",
      "tokenPush": "",
    }}

    a = RestClient.post(url, payload.to_json, header) do |response|
      r = JSON.parse(response.body)
      @token = r["usuarioTO"]["token"]
    end

    #########criar request ##############
    url_vcl = "https://hmg-omnimais.omni.com.br/api/veiculo/simular"
    header_vcl = {
      "Accept": "application/json, text/plain, */*",
      "omni-autenticacao": "{\"login\":\"184TIAGO\",\"token\":\"#{@token}\"}",
      "Content-Type": "application/json",
    }
    payload_vcl = {
      "grupo1": string,
      "lojaId": "5596",
      "vendedorId": "0",
      "cpf": "03557431673",
      "dataNascimento": 170910000000,
      "ddd": "11",
      "celular": "976290517",
      "renda": "5000.00",
      "veiculos": [{
        "placa": "AWP0500",
        "uf": "PR",
        "utilizacaoAgropecuaria": false,
        "condicao": "USADO",
      }],
      "plataformaInclusao": "WEB",
    }
    a = RestClient.post(url_vcl, payload_vcl.to_json, header_vcl) do |response|
      r = JSON.parse(response.body)
      p @proposta = r["proposta"]["id"]
    end

    ############ esperar retorno ##################

    url_retorno = "https://hmg-omnimais.omni.com.br/api/veiculo/simulacao/#{@proposta}?ultimaDataTaxa=1538082978788&retorno=0&carencia=0&parcelas=0"

    loop do
      p "Aguardando resposta...."
      sleep 10
      a = RestClient.get(url_retorno, header_vcl) do |response|
        @r = JSON.parse(response.body)
        @status = @r["status"]
      end
      break unless @status == "ANALISE_PENDENTE" or @status.include?("não permitida")
    end
    puts @status
    puts @r['message']
    return @r
  end
  
end
