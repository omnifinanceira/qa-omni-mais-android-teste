Before do
  $driver.start_driver
  @wait = Selenium::WebDriver::Wait.new(:timeout => 300)
  #@driver.manage.timeouts.implicit_wait = 5
  #   @commons = Commons.new
  @api = Api.new
end

AfterStep do 
  
  screenshot = screenshot("data/screenshots/#{Time.now.strftime("%Y%m%d%H%M%S")}.png")
  embed("data:image/png;base64,#{Base64.encode64(open(screenshot).to_a.join)}", "image/png")
  
end

After do 
  #binding.pry
  $driver.driver_quit
end