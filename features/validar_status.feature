#language: pt
@status
Funcionalidade: Validações de status

Como um usuario, Eu desejo visualizar o status da ficha conforme ela vai evoluindo no processo de aprovação para que eu consiga acompanhar o desenvolvimento da mesma
@status_resultado_parcial
Cenario: Visualizar que a proposta esta no status resultado parcial
Dado que eu possua um ficha com status "1"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "RESULTADO PARCIAL"

@status_aguardando_analise_direta
Cenario: Visualizar que a proposta esta no status analise direta
Dado que eu possua um ficha com status "2"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "AGUARDANDO ANÁLISE DIRETA"

@status_em_analise_direta
Cenario: Visualizar que a proposta esta no status em analise direta
Dado que eu possua um ficha com status "3"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "EM ANÁLISE DIRETA"

@status_aprovada
Cenario: Visualizar que a proposta esta no status aprovada
Dado que eu possua um ficha com status "4"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "APROVADA"

@status_doc_necessaria
Cenario: Visualizar que a proposta esta no status documentacao necessaria
Dado que eu possua um ficha com status "5"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "DOCUMENTAÇÃO NECESSÁRIA"

@status_doc_enviada
Cenario: Visualizar que a proposta esta no status documentacao enviada
Dado que eu possua um ficha com status "6"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "DOCUMENTAÇÃO ENVIADA"

@status_em_analise
Cenario: Visualizar que a proposta esta no status em analise
Dado que eu possua um ficha com status "7"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "EM ANÁLISE"

@status_pendencia
Cenario: Visualizar que a proposta esta no status pendencia
Dado que eu possua um ficha com status "8"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "PENDENCIA"

@status_gravame
Cenario: Visualizar que a proposta esta no status inclusao de gravame
Dado que eu possua um ficha com status "9"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "INCLUSÃO DE GRAVAME"

@status_pgto_agendado
Cenario: Visualizar que a proposta esta no status pagamento agendado
Dado que eu possua um ficha com status "10"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "PAGAMENTO AGENDADO"

@status_pgto_realizado
Cenario: Visualizar que a proposta esta no status pagamento realizado
Dado que eu possua um ficha com status "11"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "PAGAMENTO REALIZADO"

@status_recusado
Cenario: Visualizar que a proposta esta no status recusada
Dado que eu possua um ficha com status "12"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "RECUSADA"

@status_recusada_formalizacao
Cenario: Visualizar que a proposta esta no status recusada formalizacao
Dado que eu possua um ficha com status "13"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "RECUSADA FORMALIZACAO"

@status_contrato_disp
Cenario: Visualizar que a proposta esta no status contrato disponivel
Dado que eu possua um ficha com status "14"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "CONTRATO DISPONÍVEL"

@status_resultado_parcial_pendente
Cenario: Visualizar que a proposta esta no status resultado parcial pendente
Dado que eu possua um ficha com status "15"
E que eu realize o login com usuario "184TIAGO"
E eu esteja na tela inicial
Quando eu pesquisar a ficha
Entao eu espero visualizar ela no status "RESULTADO PARCIAL PENDENTE"


